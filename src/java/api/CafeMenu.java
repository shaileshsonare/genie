/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import model.dao.CafeMenuDAO;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 */
@Path("cafe_menu")
public class CafeMenu {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CafeMenu
     */
    public CafeMenu() {
    }

    /**
     * Retrieves representation of an instance of api.CafeMenu
     * @param category
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@QueryParam(value = "category") String category) {
        Gson gson = new Gson();
        if (category == null) {
            return gson.toJson(new CafeMenuDAO().getCafeMenuItems());
        } else {
            return gson.toJson(new CafeMenuDAO().getCafeMenuItems(category));
        }
        
    }

    /**
     * PUT method for updating or creating an instance of CafeMenu
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
