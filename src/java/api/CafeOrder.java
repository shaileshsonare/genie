/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import database.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import jdk.nashorn.internal.ir.RuntimeNode;
import model.dao.CafeOrderDAO;
import org.eclipse.persistence.annotations.HashPartitioning;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 */
@Path("cafe_order")
public class CafeOrder {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Cafeteria
     */
    public CafeOrder() {
    }

    /**
     * Retrieves representation of an instance of api.CafeOrder
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        CafeOrderDAO codao = new CafeOrderDAO();
        Gson gson = new Gson();
        return gson.toJson(codao.getAllCafeOrderList());
    }
    
    @GET
    @Path("get_cafe_order_list")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCafeOrderList() {
        CafeOrderDAO codao = new CafeOrderDAO();
        Gson gson = new Gson();
        return gson.toJson(codao.getCafeOrderList());
    }
    
    @GET
    @Path("get_cafe_order")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCafeOrder(@QueryParam("order_id") String order_id) {
        CafeOrderDAO codao = new CafeOrderDAO();
        Gson gson = new Gson();
        return gson.toJson(codao.getCafeOrderByOrderId(Integer.parseInt(order_id)));
    }
    
    @POST
    @Path("add_cafe_order")
    @Consumes("application/x-www-form-urlencoded")
    public String addCafeOrder(final MultivaluedMap<String, String> formParams) {
        
        Gson gson = new Gson();
        int order_id = this.getOrderId();
        int inserted_id = 0;
        int menu_item_id = 1;
        String sgid = formParams.getFirst("sgid");
        int room_id = Integer.parseInt(formParams.getFirst("room_id"));
        String device_id = formParams.getFirst("device_id");
        formParams.remove("sgid");
        formParams.remove("room_id");
        formParams.remove("device_id");
        
        Session session = new Connection().getSession();
        Transaction transaction = session.beginTransaction();
        
        for(Iterator iterator= formParams.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry entry = (Map.Entry) iterator.next();
            String menu_item_name = (String) entry.getKey();
            menu_item_id = Integer.parseInt(menu_item_name.substring(menu_item_name.lastIndexOf("_")+1));
            LinkedList list = (LinkedList) entry.getValue();
            String menu_item = (String) list.getFirst();
              
            if(menu_item != null && menu_item.equals("0") == false) {  
                model.CafeOrder cafe_order = new model.CafeOrder();
                cafe_order.setId(Integer.MIN_VALUE);
                cafe_order.setOrderId(order_id);
                cafe_order.setRoomId(room_id);
                cafe_order.setMenuId(menu_item_id);
                cafe_order.setQuantity(Integer.parseInt(menu_item));
                cafe_order.setSgid(sgid);
                cafe_order.setDeviceId(device_id);
                cafe_order.setAddedDate(new Date());
                cafe_order.setStatus("pending");
                inserted_id = (Integer) session.save(cafe_order);
            }
        }
        
        transaction.commit();
        
        HashMap<String,String> hm = new HashMap<String,String>();
        
        if (inserted_id > 0) {
            hm.put("success", "1");
            hm.put("order_id", "" + order_id);
        } else {
            hm.put("success", "0");
            hm.put("exception", "" + new Exception("Error occured during insertion"));
        }
        
        return gson.toJson(hm);
    }

    /**
     * PUT method for updating or creating an instance of CafeOrder
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
    
    public int getOrderId() {
        
        int order_id = 0;
        
        Session session = new Connection().getSession();
        
        List list = session.createSQLQuery("select max(order_id) from cafe_order").list();
        
        if(list.get(0) != null) {
            Iterator itr=list.iterator();  
            while(itr.hasNext()){  
                order_id = (Integer)itr.next();
            }
        }
        
        return ++order_id;
    }
}
