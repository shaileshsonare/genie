/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import model.Booking;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 */
@Path("user_account")
public class UserAccount {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserAccount
     */
    public UserAccount() {
    }

    /**
     * Retrieves representation of an instance of api.UserAccount
     * @return an instance of java.lang.String
     */
    @GET
    @Path("login")
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        
        Booking b = new Booking();
        b.setId(25);
        
        List list = new ArrayList();
        list.add("Hello India" + b.getId().toString());
        
        Gson gson = new Gson();
        return gson.toJson(list);
        
        //turn "{'success', 1}";
    }

    /**
     * PUT method for updating or creating an instance of UserAccount
     * @param content representation for the resource
     */
    @PUT
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
