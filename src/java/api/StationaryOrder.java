/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import database.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import model.CafeOrder;
import model.dao.StationaryOrderDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 */
@Path("stationary_order")
public class StationaryOrder {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of StationaryOrder
     */
    public StationaryOrder() {
    }

    /**
     * Retrieves representation of an instance of api.StationaryOrder
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        StationaryOrderDAO sodao = new StationaryOrderDAO();
        Gson gson = new Gson();
        return gson.toJson(sodao.getAllStationaryOrderList());
    }
    
    @GET
    @Path("get_stationary_order")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@QueryParam("order_id") String order_id) {
        StationaryOrderDAO sodao = new StationaryOrderDAO();
        Gson gson = new Gson();
        return gson.toJson(sodao.getStationaryByOrderId(Integer.parseInt(order_id)));
    }
    
    @POST
    @Path("add_stationary_order")
    @Produces(MediaType.APPLICATION_JSON)
    public String addStationaryOrder(@FormParam("notebook") String notebook,
                                    @FormParam("pen") String pen,
                                    @FormParam("pencil") String pencil,
                                    @FormParam("duster") String duster,
                                    @FormParam("marker") String marker,
                                    @FormParam("room_id") String room_id,
                                    @FormParam("sgid") String sgid,
                                    @FormParam("device_id") String device_id) {
        
        int order_id = this.getOrderId();
        int inserted_id = 0;
        Session session = new Connection().getSession();
        Transaction transaction = session.beginTransaction();
        
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        if(notebook != null && notebook.equals("0") == false)
            map.put(1, Integer.parseInt(notebook));
        if(pen != null && pen.equals("0") == false)
            map.put(2, Integer.parseInt(pen));
        if(pencil != null && pencil.equals("0") == false)
            map.put(3, Integer.parseInt(pencil));
        if(duster != null && duster.equals("0") == false)
            map.put(4, Integer.parseInt(duster));
        if(marker != null && marker.equals("0") == false)
            map.put(5, Integer.parseInt(marker));
        
        for(Iterator iterator = map.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry entry = (Map.Entry)iterator.next();
            int stationary_id = (Integer) entry.getKey();
            int quantity = (Integer) entry.getValue();
            
            model.StationaryOrder stationary_order = new model.StationaryOrder();
            stationary_order.setId(Integer.MIN_VALUE);
            stationary_order.setOrderId(order_id);
            stationary_order.setRoomId(Integer.parseInt(room_id));
            stationary_order.setStationaryId(stationary_id);
            stationary_order.setQuantity(quantity);
            stationary_order.setSgid(sgid);
            stationary_order.setDeviceId(device_id);
            stationary_order.setAddedDate(new Date());
            stationary_order.setUpdatedDate(new Date());
            stationary_order.setStatus("pending");
            inserted_id = (Integer) session.save(stationary_order);
            
        }
        
        transaction.commit();
        
        Gson gson = new Gson();
        HashMap<String,String> hm = new HashMap<String,String>();
        
        if (inserted_id > 0) {
            hm.put("success", "1");
            hm.put("order_id", "" + order_id);
        } else {
            hm.put("success", "0");
            hm.put("exception", "" + new Exception("Error occured during insertion"));
        }
        
        return gson.toJson(hm);
    }
    
    public int getOrderId() {
        
        int order_id = 0;
        
        Session session = new Connection().getSession();
        
        List list = session.createSQLQuery("select max(order_id) from stationary_order").list();
        
        if(list.get(0) != null) {
            Iterator itr=list.iterator();  
            while(itr.hasNext()){  
                order_id = (Integer)itr.next();
            }
        }
        
        return ++order_id;
    }

    /**
     * PUT method for updating or creating an instance of StationaryOrder
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
