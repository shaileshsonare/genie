/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Shailesh Sonare
 */
@javax.ws.rs.ApplicationPath("api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(api.Booking.class);
        resources.add(api.CafeMenu.class);
        resources.add(api.CafeOrder.class);
        resources.add(api.Feedback.class);
        resources.add(api.MeetingRoom.class);
        resources.add(api.StationaryItems.class);
        resources.add(api.StationaryOrder.class);
        resources.add(api.UserAccount.class);
        resources.add(api.Visitors.class);
    }
    
}
