/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.dao.StationaryDAO;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 */
@Path("stationary_items")
public class StationaryItems {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of StationaryItems
     */
    public StationaryItems() {
    }

    /**
     * Retrieves representation of an instance of api.StationaryItems
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Gson gson = new Gson();
        return gson.toJson(new StationaryDAO().getAllStationaryItems());
    }

    /**
     * PUT method for updating or creating an instance of StationaryItems
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
