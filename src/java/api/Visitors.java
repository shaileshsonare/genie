/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import database.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import model.dao.VisitorsDAO;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 */
@Path("visitors")
public class Visitors {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Visitor
     */
    public Visitors() {
    }

    /**
     * Retrieves representation of an instance of api.Visitors
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        
        VisitorsDAO vdao = new VisitorsDAO();
        Gson gson = new Gson();
        return gson.toJson(vdao.getAllVisitors());
    }
    
    @GET
    @Path("getvisitor")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@QueryParam("id") String id) {
        Connection conn = new Connection();
        Session session = conn.getSession();
        
        List list = session.createQuery("FROM Visitors WHERE id = " + id).list();
        Gson gson = new Gson();
        return gson.toJson(list);
    }

/*    
    @POST
    @Path("addvisitor")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String addVisitor(final MultivaluedMap<String, String> formParams) {
        
        Gson gson = new Gson();
        model.Visitors visitor = new model.Visitors();
        
        visitor.setId(Long.MIN_VALUE);
        
        if(formParams.get("name") != null)
            visitor.setName(formParams.get("name").toString());
        
        if(formParams.get("address") != null)
            visitor.setMobileNumber(formParams.get("address").toString());
        
        if(formParams.get("mobile_number") != null)
            visitor.setMobileNumber(formParams.get("mobile_number").toString());
        
        if(formParams.get("came_from") != null)
            visitor.setCameFrom(formParams.get("came_from").toString());
        
        if(formParams.get("to_meet") != null)
            visitor.setToMeet(formParams.get("to_meet").toString());
        
        if(formParams.get("id_proof") != null)
            visitor.setIdProof(formParams.get("id_proof").toString());
        
        if(formParams.get("id_proof_pic") != null)
            visitor.setIdProofPic(formParams.get("id_proof_pic").toString());
        
        visitor.setAddedDate(new Date());
        visitor.setUpdatedDate(new Date());
        
        Connection conn = new Connection();
        Session session = conn.getSession();
        
        Transaction t = session.beginTransaction();
        
        long id = (Long) session.save(visitor);
        
        t.commit();

        
        return gson.toJson(formParams.get("name") + " " + id);
    }
    
*/
    
    @POST
    @Path("addvisitor")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String addVisitor(@FormParam("name") String name, 
            @FormParam("address") String address, 
            @FormParam("mobile_number") String mobile_number, 
            @FormParam("came_from") String came_from, 
            @FormParam("to_meet") String to_meet, 
            @FormParam("id_proof") String id_proof, 
            @FormParam("id_proof_pic") String id_proof_pic,
            @FormParam("device_id") String device_id) {
        
        Gson gson = new Gson();
        model.Visitors visitor = new model.Visitors();
        
        visitor.setId(Long.MIN_VALUE);
        
        if(name != null)
            visitor.setName(name);
        
        if(address != null)
            visitor.setAddress(address);
        
        if(mobile_number != null)
            visitor.setMobileNumber(mobile_number);
        
        if(came_from != null)
            visitor.setCameFrom(came_from);
        
        if(to_meet != null)
            visitor.setToMeet(to_meet);
        
        if(id_proof != null)
            visitor.setIdProof(id_proof);
        
        if(id_proof_pic != null)
            visitor.setIdProofPic(id_proof_pic);
        
        visitor.setDeviceId(device_id);
        visitor.setAddedDate(new Date());
        visitor.setUpdatedDate(new Date());
        
        Connection conn = new Connection();
        Session session = conn.getSession();
        Transaction t = session.beginTransaction();
        long id = (Long) session.save(visitor);
        t.commit();
        
        HashMap<String,String> hm = new HashMap<String,String>();
        
        if (id > 0) { // success
            hm.put("success", "1");
            hm.put("inserted_id", id + "");
            
            return gson.toJson(hm);
        } else {
            return "{'success':'0','reason','" + new Exception().getMessage()  + "'}";
        }
        
    }

    /**
     * PUT method for updating or creating an instance of Visitors
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
