/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import database.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import model.dao.FeedbackDAO;
import org.hibernate.Session;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 */
@Path("feedback")
public class Feedback {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Feedback
     */
    public Feedback() {
    }

    /**
     * Retrieves representation of an instance of api.Feedback
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        FeedbackDAO fdao = new FeedbackDAO();
        Gson gson = new Gson();
        return gson.toJson(fdao.getAllFeedbackList());
    }
    
    @GET
    @Path("get_feedback")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@QueryParam("id") String id) {
        FeedbackDAO feedback_dao = new FeedbackDAO();
        Gson gson = new Gson();
        return gson.toJson(feedback_dao.getFeedbackById(Integer.parseInt(id)));
    }

    @POST
    @Path("add_feedback")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String addBooking(@FormParam("subject") String subject,
                            @FormParam("description") String description,
                            @FormParam("sgid") String sgid,
                            @FormParam("device_id") String device_id) throws Exception {
        
        model.Feedback feedback = new model.Feedback();
        feedback.setId(Long.MIN_VALUE);
        if(subject != null) {
            feedback.setSubject(subject);
        }
        if(description != null) {
            feedback.setDescription(description);
        }
        if(sgid != null) {
            feedback.setSgid(sgid);
        }
        feedback.setDeviceId(device_id);
        feedback.setAddedDate(new Date());
        long inserted_id = new FeedbackDAO().addFeedback(feedback);
        
        HashMap<String,String> hm = new HashMap<String,String>();
        
        if (inserted_id > 0) {
            hm.put("success", "1");
            hm.put("inserted_id", inserted_id + "");
        } else {
            throw new Exception("Error occured during insertion" + inserted_id );
        }
        
        return new Gson().toJson(hm);
    }
    
    /**
     * PUT method for updating or creating an instance of Feedback
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
