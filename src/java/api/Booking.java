/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.google.gson.Gson;
import database.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import model.dao.BookingDAO;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * REST Web Service
 *
 * @author Shailesh Sonare
 */
@Path("booking")
public class Booking {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Booking
     */
    public Booking() {
    }

    /**
     * Retrieves representation of an instance of api.Booking
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        
        model.dao.BookingDAO bdao = new BookingDAO();
        Gson gson = new Gson();
        
        return gson.toJson(bdao.getAllBookingsList());
    }
    
    @GET
    @Path("getbooking")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@QueryParam("id") String id) {
        
        BookingDAO bdao = new BookingDAO();
        
        Gson gson = new Gson();
        return gson.toJson(bdao.getBookingById(Integer.parseInt(id)));
    }
    
    @POST
    @Path("addbooking")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public String addBooking(@FormParam("room_id") String room_id,
                            @FormParam("booking_date") String booking_date,
                            @FormParam("from_time") String from_time,
                            @FormParam("to_time") String to_time,
                            @FormParam("sgid") String sgid,
                            @FormParam("device_id") String device_id) throws ParseException {
        
        Gson gson = new Gson();
        
        model.Booking booking = new model.Booking();
        
        if(room_id != null)
            booking.setRoomId(Integer.parseInt(room_id));
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        Date from_time_dt = null;
        if(from_time != null) {
            from_time_dt = format.parse(booking_date + " " + from_time + ".10000");
            booking.setFromTime(from_time_dt);
        }
        
        Date to_time_dt = null;
        if(to_time != null) {
            to_time_dt = format.parse(booking_date + " " + to_time + ".10000");
            booking.setToTime(to_time_dt);
        }
        
        if(sgid != null) {
            booking.setSgid(sgid);
        }
        
        booking.setDeviceId(device_id);
        booking.setAddedDate(new Date());
        booking.setUpdatedDate(new Date());

        //System.out.println(format.format(fechaNueva)); // Prints 2013-10-10 10:49:29
        
        HashMap<String,String> hm = new HashMap<String,String>();
        
        List list = this.available(format.format(from_time_dt), format.format(from_time_dt), format.format(to_time_dt));
        
        if(list.size() > 0) {
            
            hm.put("success", "0");
            hm.put("reason", "already_exist");
            hm.put("booking", gson.toJson(list.iterator().next()));
            
            return gson.toJson(hm);
            
        } else {
            
            Connection conn = new Connection();
            Session session = conn.getSession();
            Transaction t = session.beginTransaction();
            int id = (Integer) session.save(booking);
            t.commit();
            
            hm.put("success", "1");
            hm.put("inserted_id", id + "");
            
            return gson.toJson(hm);
            
        }
    }
    
    public List available(String date, String from, String to) {

            String hql = "SELECT * FROM `booking` WHERE from_time BETWEEN '" + from + "' AND '" + to + "' OR to_time BETWEEN '" + from + "' AND '" + to + "'";
            Session session = new Connection().getSession();
            List<Booking> result = session.createSQLQuery(hql).list();

            return result;
    }

    /**
     * PUT method for updating or creating an instance of Booking
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
