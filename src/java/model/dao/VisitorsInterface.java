/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.Visitors;

/**
 *
 * @author Shailesh Sonare
 */
public interface VisitorsInterface extends MainDAO {
    public List<Visitors> getAllVisitors();
    public Visitors getVisitor(int id);
    public boolean updateVisitor();
    public boolean deleteVisitor();
    public void addVisitor(Visitors visitor);
}
