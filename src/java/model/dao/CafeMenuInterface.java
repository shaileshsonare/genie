/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.CafeMenu;

/**
 *
 * @author Shailesh Sonare
 */
public interface CafeMenuInterface extends MainDAO {
    public List<CafeMenu> getCafeMenuItems();
    public List<CafeMenu> getCafeMenuItems(String category);
    public CafeMenu getCafeMenuItem(int id);
    public boolean updateCafeMenuItem();
    public boolean deleteCafeMenuItem();
    public void addCafeMenuItem(CafeMenu cafemenu);
}
