/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import database.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Booking;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Shailesh Sonare
 */
public class BookingDAO implements BookingInterface {

    @Override
    public List<Booking> getAllBookings() {
        Connection conn = new Connection();
        
        Session session = conn.getSession();
        
        String hql = "FROM Booking";
        
        List booking_list = session.createQuery(hql).list();
        
        return booking_list;
    }
    
    public List getAllBookingsList() {
        Connection conn = new Connection();
        
        Session session = conn.getSession();
        
        String sql = "SELECT b.id, mr.name room, DATE_FORMAT(b.from_time, '%r') from_time, DATE_FORMAT(b.to_time, '%r') to_time, "
                + " b.sgid, CONCAT(su.first_name, ' ', su.last_name) sguser, DATE_FORMAT(b.added_date, '%d-%m-%Y %r') added_date, "
                + " d.location"
                + " FROM booking b LEFT JOIN meeting_room mr ON b.room_id = mr.id "
                + " LEFT JOIN sgusers su ON b.sgid = su.sgid "
                + " LEFT JOIN devices d ON b.device_id = d.device_id "
                + " ORDER BY added_date DESC";
        
        List list = session.createSQLQuery(sql).list();
        
        List final_result = new ArrayList();
        
        for (int i = 0; i < list.size(); i++) {
            Object[] obj = (Object[]) list.get(i);
            Map<String, String> result = new HashMap<String, String>();
            result.put("id", "" + obj[0]);
            result.put("room", "" + obj[1]);
            result.put("from_time", "" + obj[2]);
            result.put("to_time", "" + obj[3]);
            result.put("sguser", "" + (obj[5] != null ? obj[5] : obj[4]));
            result.put("added_date", "" + obj[6]);
            result.put("location", "" + obj[7]);
            final_result.add(result);
        }
        
        return final_result;
    }

    @Override
    public Booking getBooking(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean updateBooking() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteBooking() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public long addBooking(Booking booking) {
        Connection conn = new Connection();
        Session session = conn.getSession();
        Transaction t = session.beginTransaction();
        long id = (Long) session.save(booking);
        t.commit();
        return id;
    }
    
    public List getBookingById(int id) {
        Connection conn = new Connection();

        Session session = conn.getSession();

        String sql = "SELECT b.id, mr.name room, DATE_FORMAT(b.from_time, '%r') from_time, DATE_FORMAT(b.to_time, '%r') to_time, "
                + " b.sgid, CONCAT(su.first_name, ' ', su.last_name) sguser, DATE_FORMAT(b.added_date, '%d-%m-%Y %r') added_date "
                + " FROM booking b LEFT JOIN meeting_room mr ON b.room_id = mr.id "
                + " LEFT JOIN sgusers su ON b.sgid = su.sgid "
                + " WHERE b.id = :id ORDER BY added_date DESC";

        List list = session.createSQLQuery(sql).setParameter("id", id).list();

        List final_result = new ArrayList();

        for (int i = 0; i < list.size(); i++) {
            Object[] obj = (Object[]) list.get(i);
            Map<String, String> result = new HashMap<String, String>();
            result.put("id", "" + obj[0]);
            result.put("room", "" + obj[1]);
            result.put("from_time", "" + obj[2]);
            result.put("to_time", "" + obj[3]);
            result.put("sguser", "" + (obj[5] != null ? obj[5] : obj[4]));
            result.put("added_date", "" + obj[6]);
            final_result.add(result);
        }

        return final_result;

    }
    
}
