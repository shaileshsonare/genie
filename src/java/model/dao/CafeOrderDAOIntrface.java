/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.CafeOrder;

/**
 *
 * @author Shailesh Sonare
 */
public interface CafeOrderDAOIntrface extends MainDAO {
    public List<CafeOrder> getAllCafeOrder();
    public CafeOrder getCafeOrder(int id);
    public boolean updateCafeOrder();
    public boolean deleteCafeOrder();
    public long addCafeOrder(CafeOrder cafe_order);
}
