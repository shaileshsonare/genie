/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import database.Connection;
import java.util.List;
import model.MeetingRoom;
import org.hibernate.Session;

/**
 *
 * @author Shailesh Sonare
 */
public class MeetingRoomDAO implements MeetingRoomInterface {

    @Override
    public List<MeetingRoom> getMeetingRoomList() {
        Session session = new Connection().getSession();
        String sql = "FROM MeetingRoom";
        return session.createQuery(sql).list();
    }

    @Override
    public MeetingRoom getMeetingRoom(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean updateMeetingRoom() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteMeetingRoom() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addMeetingRoom(MeetingRoom meeting_room) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isNull() {
        return false;
    }
    
}
