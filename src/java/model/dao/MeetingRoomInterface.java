/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.MeetingRoom;

/**
 *
 * @author Shailesh Sonare
 */
public interface MeetingRoomInterface extends MainDAO {
    public List<MeetingRoom> getMeetingRoomList(); 
    public MeetingRoom getMeetingRoom(int id);
    public boolean updateMeetingRoom();
    public boolean deleteMeetingRoom();
    public void addMeetingRoom(MeetingRoom meeting_room);
}
