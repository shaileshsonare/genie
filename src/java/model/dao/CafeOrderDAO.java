/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import database.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.CafeOrder;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Shailesh Sonare
 */
public class CafeOrderDAO implements CafeOrderDAOIntrface {

    @Override
    public List<CafeOrder> getAllCafeOrder() {
        Connection conn = new Connection();
        
        Session session = conn.getSession();
        
        String hql = "FROM CafeOrder ORDER BY addedDate DESC";
        List cafe_order_list = session.createQuery(hql).list();
        
        return cafe_order_list;
    }
    
    public List getAllCafeOrderList() {
        Connection conn = new Connection();
        Session session = conn.getSession();
        
        String sql = "SELECT co.id, co.order_id, mr.name room, cm.name menu_item, co.quantity, "
                + "co.sgid, CONCAT(su.first_name, ' ', su.last_name) sguser, DATE_FORMAT(co.added_date, '%d-%m-%Y %r') added_date, "
                + "d.location, co.status "
                + "FROM cafe_order co LEFT JOIN meeting_room mr ON co.room_id = mr.id "
                + "LEFT JOIN cafe_menu cm ON co.menu_id = cm.id "
                + "LEFT JOIN sgusers su ON co.sgid = su.sgid "
                + "LEFT JOIN devices d ON co.device_id = d.device_id "
                + "ORDER BY added_date DESC";
        
        List list = session.createSQLQuery(sql).list();
        
        List final_result = new ArrayList();
        
        for (int i = 0; i < list.size(); i++) {
            Object[] obj = (Object[]) list.get(i);
            Map<String, String> result = new HashMap<String, String>();
            result.put("id", "" + obj[0]);
            result.put("order_id", "" + obj[1]);
            result.put("room", "" + obj[2]);
            result.put("menu_item", "" + obj[3]);
            result.put("quantity", "" + obj[4]);
            result.put("sguser", "" + (obj[6] != null ? obj[6] : obj[5]));
            result.put("ordered_date", "" + obj[7]);
            result.put("location", "" + obj[8]);
            result.put("status", "" + obj[9]);
            final_result.add(result);
        }
        
        return final_result;
    }
    
    public List getCafeOrderList() {
        Connection conn = new Connection();
        Session session = conn.getSession();
        
        String sql = "SELECT co.id, co.order_id, mr.name room, cm.name menu_item, co.quantity, "
                + "co.sgid, CONCAT(su.first_name, ' ', su.last_name) sguser, DATE_FORMAT(co.added_date, '%d-%m-%Y %r') added_date, "
                + "d.location, co.status "
                + "FROM cafe_order co LEFT JOIN meeting_room mr ON co.room_id = mr.id "
                + "LEFT JOIN cafe_menu cm ON co.menu_id = cm.id "
                + "LEFT JOIN sgusers su ON co.sgid = su.sgid "
                + "LEFT JOIN devices d ON co.device_id = d.device_id "
                + "ORDER BY added_date DESC";

        List list = session.createSQLQuery(sql).list();
        
        List final_result = new ArrayList();
        
        for (int i = 0; i < list.size(); i++) {
            Object[] obj = (Object[]) list.get(i);
            Map<String, String> result = new HashMap<String, String>();
            result.put("id", "" + obj[0]);
            result.put("order_id", "" + obj[1]);
            result.put("room", "" + obj[2]);
            result.put("menu_item", "" + obj[3]);
            result.put("quantity", "" + obj[4]);
            result.put("sguser", "" + (obj[6] != null ? obj[6] : obj[5]));
            result.put("ordered_date", "" + obj[7]);
            result.put("location", "" + obj[8]);
            result.put("status", "" + obj[9]);
            final_result.add(result);
        }
        
        return final_result;
    }

    @Override
    public CafeOrder getCafeOrder(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean updateCafeOrder() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteCafeOrder() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long addCafeOrder(CafeOrder cafe_order) {
        Connection conn = new Connection();
        Session session = conn.getSession();
        Transaction t = session.beginTransaction();
        long id = (Long) session.save(cafe_order);
        t.commit();
        return id;
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isNull() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object getCafeOrderByOrderId(int order_id) {
        Connection conn = new Connection();
        
        Session session = conn.getSession();
        
        String sql = "SELECT co.id, co.order_id, mr.name room, cm.name menu_item, co.quantity, "
                + "co.sgid, CONCAT(su.first_name, ' ', su.last_name) sguser, DATE_FORMAT(co.added_date, '%d-%m-%Y %r') added_date, "
                + "d.location, co.status "
                + "FROM cafe_order co LEFT JOIN meeting_room mr ON co.room_id = mr.id "
                + "LEFT JOIN cafe_menu cm ON co.menu_id = cm.id "
                + "LEFT JOIN sgusers su ON co.sgid = su.sgid "
                + "LEFT JOIN devices d ON co.device_id = d.device_id "
                + "WHERE co.order_id = :orderId ORDER BY added_date DESC";
        
        List list = session.createSQLQuery(sql).setParameter("orderId", order_id).list();
        
        List final_result = new ArrayList();
        
        for (int i = 0; i < list.size(); i++) {
            Object[] obj = (Object[]) list.get(i);
            Map<String, String> result = new HashMap<String, String>();
            result.put("id", "" + obj[0]);
            result.put("order_id", "" + obj[1]);
            result.put("room", "" + obj[2]);
            result.put("menu_item", "" + obj[3]);
            result.put("quantity", "" + obj[4]);
            result.put("sguser", "" + (obj[6] != null ? obj[6] : obj[5]));
            result.put("ordered_date", "" + obj[7]);
            result.put("location", "" + obj[8]);
            result.put("status", "" + obj[9]);
            final_result.add(result);
        }
        
        
        return final_result;
    }
    
    public String changeStatus(int order_id, String status) {
        String hql = "UPDATE CafeOrder set status = :status, updated_date = :updated_date "
                + "WHERE orderId = :order_id";
        
        Session session = new Connection().getSession();
        Transaction t = session.beginTransaction();
        
        Query query = session.createQuery(hql);
        query.setParameter("status", status);
        query.setParameter("order_id", order_id);
        query.setParameter("updated_date", new Date());
        int result = query.executeUpdate();
        
        t.commit();
        return "Rows affected: " + result;
    }
}
