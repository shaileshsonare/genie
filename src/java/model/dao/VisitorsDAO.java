/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import database.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Visitors;
import org.hibernate.Session;

/**
 *
 * @author Shailesh Sonare
 */
public class VisitorsDAO implements VisitorsInterface {

    @Override
    public List<Visitors> getAllVisitors() {
        
        Connection conn = new Connection();
        
        Session session = conn.getSession();
        
        String hql = "FROM Visitors ORDER BY addedDate DESC";
        
        List visitors_list = session.createQuery(hql).list();
        
        return visitors_list;
    }

    @Override
    public Visitors getVisitor(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean updateVisitor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteVisitor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addVisitor(Visitors visitor) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isNull() {
        return false;
    }
    
}
