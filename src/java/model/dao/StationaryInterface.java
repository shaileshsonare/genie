/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.Stationary;

/**
 *
 * @author Shailesh Sonare
 */
public interface StationaryInterface extends MainDAO {
    public List<Stationary> getAllStationaryItems();
    public Stationary getStationary(int id);
    public boolean updateStationary();
    public boolean deleteStationary();
    public void addStationary(Stationary stationary);
}
