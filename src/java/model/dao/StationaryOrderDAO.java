/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import api.StationaryOrder;
import database.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Shailesh Sonare
 */
public class StationaryOrderDAO implements StationaryOrderDAOInterface {

    @Override
    public List<StationaryOrder> getAllStationaryOrder() {
        Connection conn = new Connection();
        
        Session session = conn.getSession();
        
        String hql = "FROM StationaryOrder ORDER BY orderId DESC";
        
        List stationary_orders_list = session.createQuery(hql).list();
        
        return stationary_orders_list;
    }
    
    public List getAllStationaryOrderList() {
        Connection conn = new Connection();
        
        Session session = conn.getSession();
        
        String sql = "SELECT so.id, so.order_id, mr.name room, s.name stationary_name, "
                + " so.quantity, so.sgid, CONCAT(su.first_name, ' ', su.last_name) sguser, DATE_FORMAT(so.added_date, '%d-%m-%Y %r') added_date, "
                + " d.location, so.status"
                + " FROM stationary_order so LEFT JOIN meeting_room mr ON so.room_id = mr.id "
                + " LEFT JOIN stationary s ON so.stationary_id = s.id "
                + " LEFT JOIN sgusers su ON so.sgid = su.sgid "
                + " LEFT JOIN devices d ON so.device_id = d.device_id "
                + " ORDER BY added_date DESC";
        
        List list = session.createSQLQuery(sql).list();
        
        List final_result = new ArrayList();
        
        for (int i = 0; i < list.size(); i++) {
            Object[] obj = (Object[]) list.get(i);
            Map<String, String> result = new HashMap<String, String>();
            result.put("id", "" + obj[0]);
            result.put("order_id", "" + obj[1]);
            result.put("room", "" + obj[2]);
            result.put("stationary_name", "" + obj[3]);
            result.put("quantity", "" + obj[4]);
            result.put("sguser", "" + (obj[6] != null ? obj[6] : obj[5]));
            result.put("added_date", "" + obj[7]);
            result.put("location", "" + obj[8]);
            result.put("status", "" + obj[9]);
            final_result.add(result);
        }
        
        return final_result;
    }
    
    public List getStationaryByOrderId(int order_id) {
        Connection conn = new Connection();
        Session session = conn.getSession();
        
        String sql = "SELECT so.id, so.order_id, so.room_id, so.stationary_id, s.name, "
                + "so.quantity, so.added_date, so.updated_date, so.sgid "
                + "FROM stationary_order so LEFT JOIN stationary s ON so.stationary_id = s.id "
                + "WHERE order_id = '" + order_id + "' ORDER BY so.stationary_id";
        
        List list = session.createSQLQuery(sql).list();
        
        List final_result = new ArrayList();
        
        for (int i = 0; i < list.size(); i++) {
            Object[] obj = (Object[]) list.get(i);
            Map<String, String> result = new HashMap<String, String>();
            result.put("id", "" + obj[0]);
            result.put("order_id", "" + obj[1]);
            result.put("room_id", "" + obj[2]);
            result.put("stationary_id", "" + obj[3]);
            result.put("stationary_name", "" + obj[4]);
            result.put("quantity", "" + obj[5]);
            result.put("added_date", "" + obj[6]);
            result.put("updated_date", "" + obj[7]);
            result.put("sgid", "" + obj[8]);
            final_result.add(result);
        }
        
        return final_result;
    }

    @Override
    public StationaryOrder getStationaryOrder(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean updateStationaryOrder() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteStationaryOrder() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addStationaryOrder(StationaryOrder stationary_order) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String changeStatus(int order_id, String status) {
        String hql = "UPDATE StationaryOrder set status = :status, updated_date = :updated_date "
                + "WHERE orderId = :order_id";
        
        Session session = new Connection().getSession();
        Transaction t = session.beginTransaction();
        
        Query query = session.createQuery(hql);
        query.setParameter("status", status);
        query.setParameter("order_id", order_id);
        query.setParameter("updated_date", new Date());
        int result = query.executeUpdate();
        
        t.commit();
        return "Rows affected: " + result;
    }
}
