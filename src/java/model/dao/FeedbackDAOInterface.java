/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.Feedback;

/**
 *
 * @author Shailesh Sonare
 */
public interface FeedbackDAOInterface extends MainDAO {
    public List<Feedback> getAllFeedback();
    public Feedback getFeedback(int id);
    public boolean updateFeedback();
    public boolean deleteFeedback();
    public long addFeedback(Feedback feedback);
}
