/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import database.Connection;
import java.util.List;
import model.CafeMenu;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Shailesh Sonare
 */
public class CafeMenuDAO implements CafeMenuInterface {

    @Override
    public List<CafeMenu> getCafeMenuItems() {
        Connection conn = new Connection();
        
        Session session = conn.getSession();
        
        String hql = "FROM CafeMenu";
        
        List cafe_menu_items = session.createQuery(hql).list();
        
        return cafe_menu_items;
    }
    
    

    @Override
    public List<CafeMenu> getCafeMenuItems(String category) {
        Connection conn = new Connection();
        Session session = conn.getSession();
        String hql = "FROM CafeMenu WHERE category = '" + category + "'";
        List cafe_menu_items = session.createQuery(hql).list();
        
        return cafe_menu_items;
    }
    
    @Override
    public CafeMenu getCafeMenuItem(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean updateCafeMenuItem() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteCafeMenuItem() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCafeMenuItem(CafeMenu cafemenu) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isNull() {
        return false;
    }
    
}
