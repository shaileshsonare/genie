/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import api.StationaryOrder;
import java.util.List;

/**
 *
 * @author Shailesh Sonare
 */
public interface StationaryOrderDAOInterface {
    public List<StationaryOrder> getAllStationaryOrder();
    public StationaryOrder getStationaryOrder(int id);
    public boolean updateStationaryOrder();
    public boolean deleteStationaryOrder();
    public void addStationaryOrder(StationaryOrder stationary_order);
}
