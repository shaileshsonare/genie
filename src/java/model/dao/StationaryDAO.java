/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import database.Connection;
import java.util.List;
import model.Stationary;
import org.hibernate.Session;

/**
 *
 * @author Shailesh Sonare
 */
public class StationaryDAO implements StationaryInterface {
    
    @Override
    public List<Stationary> getAllStationaryItems() {
        Session session = new Connection().getSession();
        
        String sql = "FROM Stationary";
        
        return session.createQuery(sql).list();
    }

    @Override
    public Stationary getStationary(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean updateStationary() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteStationary() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addStationary(Stationary stationary) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isNull() {
        return false;
    }
    
}
