/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import database.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Feedback;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Shailesh Sonare
 */
public class FeedbackDAO implements FeedbackDAOInterface {

    @Override
    public List<Feedback> getAllFeedback() {
        Connection conn = new Connection();
        
        Session session = conn.getSession();
        
        String hql = "FROM Feedback";
        
        List feedback_list = session.createQuery(hql).list();
        
        return feedback_list;
    }
    
    public List getAllFeedbackList() {
        Connection conn = new Connection();

        Session session = conn.getSession();

        String sql = "SELECT f.id, f.subject, f.description, f.sgid, "
                + " CONCAT(su.first_name, ' ', su.last_name) sguser, DATE_FORMAT(f.added_date, '%d-%m-%Y %r') added_date, "
                + " d.location"
                + " FROM feedback f LEFT JOIN sgusers su ON f.sgid = su.sgid "
                + " LEFT JOIN devices d ON f.device_id = d.device_id "
                + " ORDER BY added_date DESC";

        List list = session.createSQLQuery(sql).list();

        List final_result = new ArrayList();

        for (int i = 0; i < list.size(); i++) {
            Object[] obj = (Object[]) list.get(i);
            Map<String, String> result = new HashMap<String, String>();
            result.put("id", "" + obj[0]);
            result.put("subject", "" + obj[1]);
            result.put("description", "" + obj[2]);
            result.put("sguser", "" + (obj[4] != null ? obj[4] : obj[3]));
            result.put("added_date", "" + obj[5]);
            result.put("location", "" + obj[6]);
            final_result.add(result);
        }

        return final_result;
    }
    
    public List getFeedbackById(int id) {
        Connection conn = new Connection();

        Session session = conn.getSession();

        String sql = "SELECT f.id, f.subject, f.description, f.sgid, "
                + " CONCAT(su.first_name, ' ', su.last_name) sguser, DATE_FORMAT(f.added_date, '%d-%m-%Y %r') added_date "
                + " FROM feedback f LEFT JOIN sgusers su ON f.sgid = su.sgid "
                + " WHERE f.id = :id ORDER BY added_date DESC";

        List list = session.createSQLQuery(sql).setParameter("id", id).list();

        List final_result = new ArrayList();

        for (int i = 0; i < list.size(); i++) {
            Object[] obj = (Object[]) list.get(i);
            Map<String, String> result = new HashMap<String, String>();
            result.put("id", "" + obj[0]);
            result.put("subject", "" + obj[1]);
            result.put("description", "" + obj[2]);
            result.put("sguser", "" + (obj[4] != null ? obj[4] : obj[3]));
            result.put("added_date", "" + obj[5]);
            final_result.add(result);
        }

        return final_result;
    }

    @Override
    public Feedback getFeedback(int id) {
        Connection conn = new Connection();
        Session session = conn.getSession();
        List list = session.createQuery("FROM Feedback WHERE id = " + id).list();
        Feedback feedback = (Feedback)list.iterator().next();
        return feedback;
        
    }

    @Override
    public boolean updateFeedback() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteFeedback() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long addFeedback(Feedback feedback) {
        Connection conn = new Connection();
        Session session = conn.getSession();
        Transaction t = session.beginTransaction();
        long id = (Long) session.save(feedback);
        t.commit();
        
        return id;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isNull() {
        return false;
    }
    
}
