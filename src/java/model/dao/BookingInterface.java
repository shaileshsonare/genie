/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.Booking;

/**
 *
 * @author Shailesh Sonare
 */
public interface BookingInterface extends MainDAO {
    public List<Booking> getAllBookings();
    public Booking getBooking(int id);
    public boolean updateBooking();
    public boolean deleteBooking();
    public long addBooking(Booking booking);
}
