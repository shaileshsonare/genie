package model;
// Generated 7 Nov, 2016 10:38:19 AM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * FeedbackOld generated by hbm2java
 */
public class FeedbackOld  implements java.io.Serializable {


     private Long id;
     private String subject;
     private String description;
     private String sgid;
     private Date addedDate;

    public FeedbackOld() {
    }

	
    public FeedbackOld(String subject, String description, Date addedDate) {
        this.subject = subject;
        this.description = description;
        this.addedDate = addedDate;
    }
    public FeedbackOld(String subject, String description, String sgid, Date addedDate) {
       this.subject = subject;
       this.description = description;
       this.sgid = sgid;
       this.addedDate = addedDate;
    }
   
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public String getSgid() {
        return this.sgid;
    }
    
    public void setSgid(String sgid) {
        this.sgid = sgid;
    }
    public Date getAddedDate() {
        return this.addedDate;
    }
    
    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }




}


