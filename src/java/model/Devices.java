package model;
// Generated 7 Nov, 2016 10:38:19 AM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Devices generated by hbm2java
 */
public class Devices  implements java.io.Serializable {


     private Integer id;
     private String deviceId;
     private String location;
     private String sgid;
     private Date addedDate;

    public Devices() {
    }

	
    public Devices(String deviceId, String location, Date addedDate) {
        this.deviceId = deviceId;
        this.location = location;
        this.addedDate = addedDate;
    }
    public Devices(String deviceId, String location, String sgid, Date addedDate) {
       this.deviceId = deviceId;
       this.location = location;
       this.sgid = sgid;
       this.addedDate = addedDate;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getDeviceId() {
        return this.deviceId;
    }
    
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    public String getLocation() {
        return this.location;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    public String getSgid() {
        return this.sgid;
    }
    
    public void setSgid(String sgid) {
        this.sgid = sgid;
    }
    public Date getAddedDate() {
        return this.addedDate;
    }
    
    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }




}


