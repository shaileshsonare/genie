/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.dao.CafeOrderDAO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shailesh Sonare
 */
@Controller
@RequestMapping("/cafe_order")
public class CafeOrder {
    @RequestMapping("/index")
    public ModelAndView getAllCafeOrder() {
        
        CafeOrderDAO codao = new CafeOrderDAO();
        ModelAndView model = new ModelAndView("cafe_order/index");
        model.addObject("result", codao.getAllCafeOrderList());
        
        return model;
    }
    
    @RequestMapping(value = "/change_status", method = RequestMethod.POST)
    public @ResponseBody
    String editStationaryOrder(@RequestParam("status_value") String status_value, @RequestParam("order_id") String order_id) {
        
        CafeOrderDAO codao = new CafeOrderDAO();
        
        
        return codao.changeStatus(Integer.parseInt(order_id), status_value);
    }
}
