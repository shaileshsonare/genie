/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shailesh Sonare
 */
@Controller
public class IndexController {
    
    @RequestMapping("/index")
    public ModelAndView indexAction() {
        
        ModelAndView model = new ModelAndView("index");
        return model;
    }
    
    public String getData() {
        return "String Value";
    }
}
