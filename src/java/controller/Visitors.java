/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.dao.VisitorsDAO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shailesh Sonare
 */
@Controller
@RequestMapping("/visitors")
public class Visitors {
    
    @RequestMapping("/index")
    public ModelAndView getAllVisitorsData() {
        
        VisitorsDAO vdao = new VisitorsDAO();
        ModelAndView model = new ModelAndView("visitors/index");
        model.addObject("result", vdao.getAllVisitors());
        
        return model;
    }
    
    
    public String getData() {
        return "String Value";
    }
}
