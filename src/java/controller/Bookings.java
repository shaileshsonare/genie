/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.Booking;
import model.dao.BookingDAO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shailesh Sonare
 */
@Controller
@RequestMapping("/bookings")
public class Bookings {
    
    @RequestMapping("/index")
    public ModelAndView getAllBookings(){
        
        BookingDAO bdao = new BookingDAO();
        ModelAndView model = new ModelAndView("booking/index");
        model.addObject("result", bdao.getAllBookingsList());
        return model;
        
    }
    
}
