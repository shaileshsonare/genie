/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.dao.FeedbackDAO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shailesh Sonare
 */
@Controller
@RequestMapping("/feedback")
public class Feedback {
    
    @RequestMapping("/index")
    public ModelAndView getAllFeedback() {
    
        FeedbackDAO feedback_dao = new FeedbackDAO();
        
        List list = feedback_dao.getAllFeedbackList();
        
        ModelAndView model = new ModelAndView("feedback/index");
        
        model.addObject("result", list);
        
        return model;
        
    }
    
}
