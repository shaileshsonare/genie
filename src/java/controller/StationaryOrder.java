/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.ws.rs.FormParam;
import model.dao.StationaryOrderDAO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Shailesh Sonare
 */
@Controller
@RequestMapping("/stationary_order")
public class StationaryOrder {
    
    @RequestMapping("/index")
    public ModelAndView getAllStationaryOrder() {
        
        StationaryOrderDAO sodao = new StationaryOrderDAO();
        ModelAndView model = new ModelAndView("stationary_order/index");
        model.addObject("result", sodao.getAllStationaryOrderList());
        
        return model;
    }
    
    @RequestMapping(value = "/change_status", method = RequestMethod.POST)
    public @ResponseBody
    String editStationaryOrder(@RequestParam("status_value") String status_value, @RequestParam("order_id") String order_id) {
        
        StationaryOrderDAO sodao = new StationaryOrderDAO();
        
        return sodao.changeStatus(Integer.parseInt(order_id), status_value);
    }
}
