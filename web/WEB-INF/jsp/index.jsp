<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%

String site_url = "http://shaileshsonare.com:8080/genie/";

response.sendRedirect(site_url + "cafe_order/index.htm");

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <a href="<%=site_url%>visitors/index.htm">Visitors List</a><br/>
        <a href="<%=site_url%>bookings/index.htm">Booking List</a><br/>
        <a href="<%=site_url%>feedback/index.htm">Feedback List</a><br/>
        <a href="<%=site_url%>stationary_order/index.htm">Stationary Order List</a><br/>
        <a href="<%=site_url%>cafe_order/index.htm">Cafeteria Order List</a><br/>
    </body>
</html>
