<%-- 
    Document   : index
    Created on : 19 Oct, 2016, 2:31:20 PM
    Author     : Shailesh Sonare
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="now" class="java.util.Date"/>

<%@ include file="../header.jsp" %>
    <div>
        <h3>Cafeteria Order List</h3>
    </div>
    <div id="result">
        <table border="1">
        <thead>
            <th>Id</th>
            <th>Order Id</th>
            <th>Room</th>
            <th>Location</th>
            <th>Cafe Menu Item</th>
            <th>Quantity</th>
            <th>Order By</th>
            <th>Ordered Date</th>
            <th>Status</th>
            <th>Action</th>
        </thead>
        <c:forEach items="${result}" var="cafe_order">
            <tr>
                <td>${cafe_order.id}</td>
                <td>${cafe_order.order_id}</td>
                <td>${cafe_order.room}</td>
                <td>${cafe_order.location}</td>
                <td>${cafe_order.menu_item}</td>
                <td>${cafe_order.quantity}</td>
                <td>${cafe_order.sguser}</td>
                <td>
                    ${cafe_order.ordered_date}
                    <!--
                    <fmt:parseDate value="${fn:substring(cafe_order.ordered_date, 0, 10)}" var="formattedDate" pattern="dd-MM-yyyy" />
                    <fmt:formatDate pattern="yyyy-MM-dd" value="${now}" var="current_date"/>
                    <c:if test="${current_date lt formattedDate}"> 
                    </c:if>
                        <br/>
                    ${formattedDate}<br/>
                    ${now}
                    -->
                </td>
                <td>${cafe_order.status == 'pending' ? cafe_order.status.concat('...') : cafe_order.status}</td>
                <td>
                    <div class="edit_button">Edit</div>
                    <div class="edit_action" style="display:none;">
                        <select onchange="changeStatus(this, ${cafe_order.order_id})">
                            <option value="pending" ${cafe_order.status == 'pending' ? 'selected' : ''}>Pending</option>
                            <option value="complete" ${cafe_order.status == 'complete' ? 'selected' : ''}>Complete</option>
                            <option value="cancel" ${cafe_order.status == 'cancel' ? 'selected' : ''}>Cancel</option>
                        </select>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
    </div>
<%@ include file="../footer.jsp" %>
<script>
    editing_flag = false;
    
    setTimeout(function(){
        if(editing_flag == false) {
            window.location.reload(1);
        }
    }, 60000);
    
    $(document).ready(function(){
       $('.edit_action').hide();
    });
    
    $('.edit_button').click(function() { 
       editing_flag = true; 
       $(this).next('.edit_action').show(); 
       $(this).hide();
    });
    
    function changeStatus(obj, order_id) {
       
       $.ajax({
           url: "<c:url value="/cafe_order/change_status.htm" />",
           method: "POST",
           data: {"status_value" : $(obj).val(), "order_id" : order_id},
       }).done(function(data){
           console.log(data);
           window.location.reload(1);
       });
       
    }
</script>