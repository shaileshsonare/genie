<%-- 
    Document   : header
    Created on : 6 Nov, 2016, 1:41:10 PM
    Author     : Shailesh Sonare 
--%>

<%

String site_url = "http://shaileshsonare.com:8080/genie/";

%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Office Genie</title>
        <link href="<c:url value="/resources/default_theme/css/default.css" />" rel="stylesheet">
    </head>
    <body>
    <div id="header">
        <img id="website-logo" src="<c:url value="/resources/images/logo.png" />" />
        <h2>Wecome to SG Genie</h2>
    </div>
    <div id="breadcrumb">
        <ul class="topnav" id="myTopnav">
            <li><a href="<c:url value="/cafe_order/index.htm" />">Cafeteria Order</a></li>
            <li><a href="<c:url value="/bookings/index.htm"/>">Meeting Room Booking</a></li>
            <li><a href="<c:url value="/stationary_order/index.htm"/>">Stationary Order</a></li>
            <li><a href="<c:url value="/visitors/index.htm"/>">Visitors</a></li>
            <li><a href="<c:url value="/feedback/index.htm"/>">Feedback</a></li>
        </ul>
    </div>

