<%-- 
    Document   : index
    Created on : 19 Oct, 2016, 2:31:20 PM
    Author     : Shailesh Sonare
--%>

<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%@ include file="../header.jsp" %>
<div>
    <h3>Feedback List</h3>
</div>
<div id="result">
    <table border="1">
        <thead>
        <th>Id</th>
        <th>Subject</th>
        <th>Description</th>
        <th>SGID</th>
        <th>Location</th>
        <th>Added Date</th>
        </thead>
        <c:forEach items="${result}" var="feedback">
            <tr>
                <td>${feedback.id}</td>
                <td>${feedback.subject}</td>
                <td>${feedback.description}</td>
                <td>${feedback.sguser}</td>
                <td>${feedback.location}</td>
                <td>${feedback.added_date}</td>
            </tr>
        </c:forEach>
    </table>
</div>
<%@ include file="../footer.jsp" %>

