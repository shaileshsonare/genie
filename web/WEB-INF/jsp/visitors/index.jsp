<%-- 
    Document   : index
    Created on : 19 Oct, 2016, 2:31:20 PM
    Author     : Shailesh Sonare
--%>

<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%@ include file="../header.jsp" %>
<div>
    <h3>Visitors List</h3>
</div>
<div id="result">
    <table border="1">
        <thead>
        <th>Id</th>
        <th>Name</th>
        <th>Address</th>
        <th>Mobile Number</th>
        <th>Came From</th>
        <th>To Meet</th>
        <th>Id Proof</th>
        <th>Avatar</th>
        <th>Id Proof Pic</th>
        <th>Added Date</th>
        <th>Updated Date</th>
        </thead>
        <c:forEach items="${result}" var="visitor">
            <tr>
                <td>${visitor.id}</td>
                <td>${visitor.name}</td>
                <td>${visitor.address}</td>
                <td>${visitor.mobileNumber}</td>
                <td>${visitor.cameFrom}</td>
                <td>${visitor.toMeet}</td>
                <td>${visitor.idProof}</td>
                <td>${visitor.avatar}</td>
                <td>${visitor.idProofPic}</td>
                <td>${visitor.addedDate}</td>
                <td>${visitor.updatedDate}</td>
            </tr>
        </c:forEach>
    </table>
</div>
<%@ include file="../footer.jsp" %>
