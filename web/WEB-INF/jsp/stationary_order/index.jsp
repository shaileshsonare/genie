<%-- 
    Document   : index
    Created on : 19 Oct, 2016, 2:31:20 PM
    Author     : Shailesh Sonare
--%>

<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%@ include file="../header.jsp" %>
<div>
    <h3>Stationary Order List</h3>
</div>
<div id="result">
        <table border="1">
            <thead>
                <th>Id</th>
                <th>Order Id</th>
                <th>Room</th>
                <th>Location</th>
                <th>Stationary Id</th>
                <th>Quantity</th>
                <th>SGID</th>
                <th>Added Date</th>
                <th>Status</th>
                <th>Action</th>
            </thead>
            <c:forEach items="${result}" var="stationary_order">
                <tr>
                    <td>${stationary_order.id}</td>
                    <td>${stationary_order.order_id}</td>
                    <td>${stationary_order.room}</td>
                    <td>${stationary_order.location}</td>
                    <td>${stationary_order.stationary_name}</td>
                    <td>${stationary_order.quantity}</td>
                    <td>${stationary_order.sguser}</td>
                    <td>${stationary_order.added_date}</td>
                    <td>${stationary_order.status == 'pending' ? stationary_order.status.concat('...') : stationary_order.status}</td>
                    <td>
                        <div class="edit_button button">Edit</div>
                        <div class="edit_action" style="display:none;">
                            <select onchange="changeStatus(this, ${stationary_order.order_id})">
                                <option value="pending" ${cafe_order.status == 'pending' ? 'selected' : ''}>Pending</option>
                                <option value="complete" ${cafe_order.status == 'complete' ? 'selected' : ''}>Complete</option>
                                <option value="cancel" ${cafe_order.status == 'cancel' ? 'selected' : ''}>Cancel</option>
                            </select>
                        </div>
                    </td>
                </tr>
            </c:forEach>
        </table>
</div>
<%@ include file="../footer.jsp" %>
<script>
    editing_flag = false;
    
    setTimeout(function(){
        if(editing_flag == false) {
            window.location.reload(1);
        }
    }, 60000);
    
    $(document).ready(function(){
       $('.edit_action').hide();
    });
    
    $('.edit_button').click(function() { 
       editing_flag = true; 
       $(this).next('.edit_action').show(); 
       $(this).hide();
    });
    
    function changeStatus(obj, order_id) {
       
       $.ajax({
           url: "<c:url value="/stationary_order/change_status.htm" />",
           method: "POST",
           data: {"status_value" : $(obj).val(), "order_id" : order_id},
       }).done(function(data){
           console.log(data);
           window.location.reload(1);
       });
       
    }
</script>