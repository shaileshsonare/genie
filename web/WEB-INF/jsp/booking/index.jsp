<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="../header.jsp" %>
<div>
    <h3>Meeting Room Booking List</h3>
</div>
<div id="result">
    <table border="1">
        <thead>
        <th>Id</th>
        <th>Room</th>
        <th>Location</th>
        <th>From Time</th>
        <th>To Time</th>
        <th>Booked By</th>
        <th>Added Date</th>
        </thead>
        <c:forEach items="${result}" var="booking">
            <tr>
                <td>${booking.id}</td>
                <td>${booking.room}</td>
                <td>${booking.location}</td>
                <td>${booking.from_time}</td>
                <td>${booking.to_time}</td>
                <td>${booking.sguser}</td>
                <td>${booking.added_date}</td>
            </tr>
        </c:forEach>
    </table>
</div>
<%@ include file="../footer.jsp" %>
